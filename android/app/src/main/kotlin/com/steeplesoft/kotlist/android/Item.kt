package com.steeplesoft.kotlist.common

//@Serializable
data class Item(
        var id: Int = -1,
        var description: String? = null,
        var completed: Boolean = false
)