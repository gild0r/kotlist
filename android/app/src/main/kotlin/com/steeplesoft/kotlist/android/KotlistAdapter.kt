package com.steeplesoft.kotlist.android

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import com.steeplesoft.kotlist.common.Item
import org.jetbrains.anko.checkBox
import org.jetbrains.anko.linearLayout
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.textView

class KotlistAdapter(val activity: Activity, var items: List<Item>) :
        ArrayAdapter<Item>(activity, 0) {
//    var list : List<Map<String, Any>> = activity.getDataFromProvider()
    var handleClick: ((item : Item) -> Unit)? = null

    override fun getView(i: Int, v: View?, parent: ViewGroup?): View {
        val item = getItem(i)
        return with(parent!!.context) {
            linearLayout {
                orientation = LinearLayout.HORIZONTAL
                checkBox {
                    isChecked = item.completed
                    onClick { cb ->
                        item.completed = isChecked
                        handleClick?.invoke(item)
                    }
                }
                textView(item.description) {
                    textSize = 16f
                }.lparams(width = matchParent, height = matchParent)
            }
        }
    }



    override fun getItem(position: Int): Item {
        return items[position]
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id.toLong();
    }

    fun updateItems(items : List<Item>) {
        this.items = items
        notifyDataSetChanged()
    }
}