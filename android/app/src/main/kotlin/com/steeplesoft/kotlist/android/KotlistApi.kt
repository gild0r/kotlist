package com.steeplesoft.kotlist.android

import com.google.gson.GsonBuilder
import com.steeplesoft.kotlist.common.Item
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Created by jdlee on 12/5/2017.
 */
interface KotlistApi {
    @GET("/items")
    fun getAll(): Call<List<Item>>

    @POST("/items/{id}")
    fun updateItem(@Path("id") id : Int , @Body item : Item) : Call<Item>

    companion object {
        const val END_POINT = "http://10.0.2.2:8181"
        const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"

        fun create(): KotlistApi {
            val gson = GsonBuilder()
                    .setDateFormat(DATE_FORMAT)
                    .setLenient()
                    .create()

            val client = OkHttpClient.Builder()
                    .addInterceptor { chain ->
                        val newRequest = chain.request()
                                .newBuilder()
                                .addHeader("Accept", "application/json")
                                .build()
                        chain.proceed(newRequest)
                    }
                    .build()

            val retrofit = Retrofit.Builder()
                    .client(client)
                    .baseUrl(KotlistApi.END_POINT)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()

            return retrofit.create(KotlistApi::class.java)
        }
    }
}