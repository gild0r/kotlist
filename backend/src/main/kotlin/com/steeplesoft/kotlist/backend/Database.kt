package com.steeplesoft.kotlist.backend

import com.steeplesoft.kotlist.common.Item
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.Application
import io.ktor.application.log
import kotlinx.coroutines.experimental.newFixedThreadPoolContext
import kotlinx.coroutines.experimental.run
import org.jetbrains.squash.connection.DatabaseConnection
import org.jetbrains.squash.connection.transaction
import org.jetbrains.squash.definition.TableDefinition
import org.jetbrains.squash.definition.autoIncrement
import org.jetbrains.squash.definition.bool
import org.jetbrains.squash.definition.integer
import org.jetbrains.squash.definition.primaryKey
import org.jetbrains.squash.definition.varchar
import org.jetbrains.squash.dialects.h2.H2Connection
import org.jetbrains.squash.expressions.count
import org.jetbrains.squash.expressions.eq
import org.jetbrains.squash.query.select
import org.jetbrains.squash.query.where
import org.jetbrains.squash.results.get
import org.jetbrains.squash.statements.fetch
import org.jetbrains.squash.statements.insertInto
import org.jetbrains.squash.statements.query
import org.jetbrains.squash.statements.set
import org.jetbrains.squash.statements.update
import org.jetbrains.squash.statements.values
import kotlin.coroutines.experimental.CoroutineContext

class Database(application: Application) {
    private val dispatcher: CoroutineContext
    private val connectionPool: HikariDataSource
    private val connection: DatabaseConnection

    init {
        val config = application.environment.config.config("database")
        val url = config.property("connection").getString()
        val poolSize = config.property("poolSize").getString().toInt()
        application.log.info("Connecting to database at '$url'")

        dispatcher = newFixedThreadPoolContext(poolSize, "database-pool")
        val cfg = HikariConfig()
        cfg.jdbcUrl = url
        cfg.maximumPoolSize = poolSize
        cfg.validate()

        connectionPool = HikariDataSource(cfg)

        connection = H2Connection { connectionPool.connection }
        connection.transaction {
            databaseSchema().create(listOf(Items))
        }
    }

    suspend fun getItems(): List<Item> = run(dispatcher) {
        connection.transaction {
            Items.select()
                    .execute().map {
                Item().apply {
                    id = it.get<Int>("id")
                    description = it.get<String>("description")
                    completed = it.get<Boolean>("completed")
                }
            }.toList()
        }
    }

    suspend fun getItem(id: String): Items = run(dispatcher) {
        connection.transaction {
            val row = Items.select()
                    .where { Items.id eq id.toInt() }
                    .execute()
            if (row.count() == 0) {
                throw RuntimeException("com.steeplesoft.kotlist.backend.Item not found")
            }
            row.single().get<Items>(0)
        }
    }

    suspend fun saveItem(item: Item): Item = run(dispatcher) {
        connection.transaction {
            println(item)
            val count = Items.select { Items.id.count() }
                    .where { Items.id eq item.id }
                    .execute().single().get<Int>(0)
            if (count == 0) {
                val insert = insertInto(Items).values {
                    it[description] = item.description
                    it[completed] = item.completed
                }
                val ex = insert.fetch(Items.id)
                        .execute()
                item.id = ex
            } else {
                update(Items).where { (Items.id eq item.id) }.set {
                    it[completed] = item.completed
                    it[description] = item.description
                }.execute()
            }
        }
        item
    }
}

object Items : TableDefinition() {
    val id = integer("id").autoIncrement().primaryKey()
    val description = varchar("description", 255)
    val completed = bool("completed")
}

