package com.steeplesoft.kotlist.backend

import com.google.gson.GsonBuilder
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.AutoHeadResponse
import io.ktor.features.CallLogging
import io.ktor.features.DefaultHeaders
import io.ktor.features.Compression
import io.ktor.features.ConditionalHeaders
import io.ktor.features.ContentNegotiation
import io.ktor.features.PartialContentSupport
import io.ktor.gson.GsonConverter
import io.ktor.http.ContentType
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get

fun Application.main() {
    val database = Database(this)

    install(DefaultHeaders)
    install(ConditionalHeaders)
    install(Compression)
    install(PartialContentSupport)
    install(AutoHeadResponse)
    install(CallLogging)
    install(ContentNegotiation) {
        register(ContentType.Application.Json,
                GsonConverter(GsonBuilder().setPrettyPrinting().serializeNulls().create()))
    }

    install(Routing) {
        get("/") {
            call.respondText("My Example Blog 3", ContentType.Text.Html)
        }
        api(database)
    }
}

//fun com.steeplesoft.kotlist.backend.main(args: Array<String>) {
//    embeddedServer(Netty, commandLineEnvironment(args)).start()
//}